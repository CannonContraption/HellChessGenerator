#!/usr/bin/env python3

import random;

srand = random.SystemRandom();

level = input("How hard do you want your game to be? (high, medium, low) ");
limit = 16;
if(level == "high"):        
    slot = srand.randrange(limit)+1;
elif(level == "medium"):
    slot = srand.randrange(limit/2)+1;
elif(level == "low"):
    slot = srand.randrange(limit/4)+1;
elif(level == "dumb"):
    slot = 1;
elif(level == "dumber"):
    print("Roll a die six times and stop bothering me.");
    exit(1);
elif(level == "meta"):
    slot = srand.randrange(srand.randrange(limit));
else:
    print("Not an option.")
    exit(2);
temp_number = 0; #We create this so we can keep track of the number we just generated.
pawns = 0;
bishops = 0;
rooks = 0;
knights = 0;
kings = 0;
queens = 0;

def evalslot():
    global slot;
    global limit;
    #print(slot);
    if(slot>limit):
        slot = limit;
        if(slot <= 0): return True;
    return False;

while limit > 0:
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    pawns += temp_number;
    limit -= temp_number;
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    bishops += temp_number;
    limit -=temp_number;
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    rooks += temp_number;
    limit -= temp_number;
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    knights += temp_number;
    limit -= temp_number;
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    kings += temp_number;
    limit -= temp_number;
    if(evalslot()): break;
    temp_number = srand.randrange(slot)+1;
    queens += temp_number;
    limit -= temp_number;

print("you got")
print(str(pawns)+" pawns");
print(str(bishops)+" bishops");
print(str(rooks)+" rooks");
print(str(knights)+" knights");
print(str(kings)+" kings");
print(str(queens)+" queens");
print()
print("Good luck!");
